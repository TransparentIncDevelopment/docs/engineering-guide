# Work Management

[[_TOC_]]

## Scrum

Our teams follow the Scrum Agile methodology guided by the [official scrum guide](https://www.scrumguides.org/scrum-guide.html) with deviations determined by the team and the scrum leader(s). 

We follow scrum's two-week sprint format. The first week of the sprint is known as the "front week"; the second week as the "back week".

### Meeting Summary

|                     | Standup       | Planning     | Backlog Grooming | Status Update | Retrospective  | Sprint Review        |
|---------------------|---------------|--------------|------------------|---------------|----------------|----------------------|
| **Official Agile?** | Y             | Y            | Y                | N             | Y              | Y                    |
| **Timing**          | Daily (am)    | 1 Per Sprint | 1 Per Sprint     | 1 Per Sprint  | 1 Per Sprint   | Not currently active |
| **Duration**        | 15 min        | 60 min       | 60 min           | 30-60 min     | 60 min         | 60 min               |
| **Owner**           | Scrum leads   | Scrum leads  | Scrum leads      | Scrum leads   | Team member(s) | Scrum leads          |
| ----------------    | ------------- |--------------|------------------|---------------|----------------|----------------------|

#### Sprint Planning Meeting
**Cadence:** Once per sprint. Ideally on the front Monday of the sprint to be planned, or the back Thursday/Friday of the preceding sprint. There should be a ~2 business day offset between sprint grooming and planning.
**Purpose:** Planning session for the team to plan the work of the next sprint. Select a set of product backlog items/bugs that are children of the highest prioritized features that can be delivered in the sprint and how that work will be achieved.

**Agenda:**
* Review the last sprints items and move them and take the following actions
  - move items from last sprint to the new sprint if still a priority
  - close if done
  - move to the backlog if not a currently needed
* Review the team’s velocity. Decide on a velocity for the next sprint. (holidays, planned vacation may influence what team takes into the sprint)
* Pull sized items into the sprint based on feature level priority, till the team’s capacity for the sprit is reached. If an item is not sized - estimate and size it.
* Check with team for consensus on the sprint commitment. [finger confidence](https://heymac.biz/2011/08/19/fist-of-five-voting-for-team-consensus/)
* **Optional:** The Scrum leads discuss the product backlog as it stands, offer time to talk through the timeline/dates.

**Result:**
Product Backlog Items representing the highest priority features loaded in the current sprint. All product backlog items in the sprint sized. Team commitment to loaded sprint items.

#### Backlog Grooming Meeting
**Owner:** Scrum leaders

**Cadence:** Once per sprint. Ideally on the back Thursday of the sprint, to prep for the coming sprint. There should be a ~2 business day offset between backlog grooming and sprint planning.

**Purpose:** To ensure that the backlog remains populated with items that are relevant, detailed and estimated to a degree appropriate with their priority

**Leads level Agenda:**
* Removing features that no longer appear relevant
* Creating new features in response to newly discovered needs
* Re-assessing the relative priority of features

**Team level Agenda**
* Breakdown of ready features into backlog items
* assigning estimates to product backlog items which have yet to receive one
* Backlog priority should be coming from the Feature level.

**Result** 
* Backlog items ready to take into next sprint
* A revised or confirmed feature backlog.

#### Scrum Status Update
> Note: this meeting is at the discretion of scrum leaders and is not an official agile process meeting.

**Owner:** Scrum leaders

**Cadence:** Once per sprint.

**Attendees:** VP of Engineering or delegate, Scrum leader(s)

**Purpose:** Provide an update to senior Engineering management about the status, velocity, and any blockers or requests to faciliate the work of the scrum leaders. 


#### Sprint Review (demo)
> Note: this is an official scrum process meeting, but is held at the discretion of scrum leaders. We are not actively scheduling this meeting a this time. This meeting can be rolled into the Backlog grooming and/or Scrum Status Update.

**Owner:** N/A - not currently performed

**Cadence:** Once per sprint.

**Purpose:** A Sprint Review is held at the end of the Sprint to inspect the Increment and adapt the Product Backlog if needed.

**Attendees:** Scrum leaders, team members, Director of Product. Optional: VP of Engineering or delegate.

**Agenda**

* Team explains what items have been “done” during the sprint. This should only be items at the feature level. Only features should be demoed during this meeting, and only features that are “Done”
* The Team demos the done features and answers any questions. Features need to conform to our definition of done.
* Collaboration on what is next if features need to be re-ordered or new features need to be created.

**Result:**  A revised or confirmed feature backlog.


#### Daily Scrum Meeting
**Owner:** Scrum leaders

**Purpose:** Planning meeting team plans work for the next 24 hours.  

**Attendees:** Scrum leaders, team members. Optional: VP of Engineering or delegate, Director of Product

**Agenda:** The structure of the meeting is set by the Development Team and can be conducted in different ways if it focuses on progress toward the Sprint Goals.

Here is an example of what might be used:

* What did I do yesterday that helped the Development Team meet the Sprint Goals?
* What will I do today to help the Development Team meet the Sprint Goals?
* Do I see any impediment that prevents me or the Development Team from meeting the Sprint Goals?

It is recommended that the scrum leader use the ADO board to focus the conversation by-backlog item. 

**Result** Plan in place for next 24 hours in place

#### Sprint Retrospective Meeting
**Owner:** Team member (not scrum leader)

**Purpose:** Discuss the sprint being completed and plan ways to increase quality, effectiveness, and morale.

**Cadence:** Once per sprint. Ideally on the back Thursday of the sprint.

**Attendees:** Scrum leaders, team members. Optional: VP of Engineering or delegate.

**Agenda**
* Team members create cards for items that went well, and not well in the sprint being finished.
* Team discusses what went well during the Sprint, what problems it encountered, and how those problems were (or were not) solved.
* Any action items are noted. 

**Result**
* Discussion + next steps documented in a Miro board
* Next step(s) created in ADO

See also: [Scrum Retrospective](https://scrumguides.org/scrum-guide.html#sprint-retrospective)

#### Sprint Review (demo)
> Note: this is an official scrum process meeting, but is held at the discretion of scrum leaders. We are not actively scheduling this meeting a this time. This meeting can be rolled into the Backlog grooming and/or Scrum Status Update.

**Owner:** N/A - not currently performed

**Cadence:** Once per sprint.

**Purpose:** A Sprint Review is held at the end of the Sprint to inspect the Increment and adapt the Product Backlog if needed.

**Attendees:** Scrum leaders, team members. Optional: VP of Engineering or delegate, Director of Product

**Agenda**

* Team explains what items have been “done” during the sprint. This should only be items at the feature level. Only features should be demoed during this meeting, and only features that are “Done”
* The Team demos the done features and answers any questions. Features need to conform to our definition of done.
* Collaboration on what is next if features need to be re-ordered or new features need to be created.

**Result:**  A revised or confirmed feature backlog.

## Roles

### Scrum Leader
The team has a rotating [Scrum Leader](https://www.scrumguides.org/scrum-guide.html#team-sm) position.
See more details [here](./scrum_leader_guide.md).

### Product Owner

The official [Product Owner](https://www.scrumguides.org/scrum-guide.html#team-po) is the Director of Product. 

Traditionally, the Product Owner role's responsibilities are distributed across the engineering team and are facilitated by the scrum leads. However, in our case the roles are combined. 

Scrum leads are responsible for understanding the priorities and considerations of the Product Owner and for translating this knowledge into their tasks. 

The Product Owner is responsible for ownership of the engineering product via:

* Product backlog accountability, including:
  - **Content** ensuring all features are defined with acceptance criteria that both engineering and business understands, works with engineering to ensure that the product backlog item break up of features accurately reflect the feature's requirements.
  - **Order** correct ordering of the feature backlog based on input from business and engineering.
  - **Value** ensure the items in the feature backlog achieve maximum business value.
  - **Understanding** ensuring that all stakeholders understand the feature backlog, and ensure all are clear about the current and future planned work.
* Collaboration, where needed, with engineering to write epics and features, and define acceptance criteria
* Representing the needs, interests, and positions of external parties (partners, customers, investors) to engineering to ensure engineering is focusing on high-value adds

## Definition of Done

### Criteria for Product Backlog Items
* Code reviewed
* Code merged to develop branch
* Engineering guide coding standards are followed
* all modifications should be covered by automated tests unless this is impossible for some reason (this would need to be justified)
* Acceptance Criteria met

### Criteria for Features
* Acceptance Criteria met
* Promoted to dev environment
* Acceptance tests pass
* Feature should be able to be delivered via CD environment
  * Xand Acceptance Tests pass
  * Ensure CI tests pass within xand-k8s
  * Xand QA Documentation needs to be updated:
    * documented feature description
    * all public interfaces documented
* Product Owner accepts feature as Done

### Criteria for Crypto related tasks
* Introduction, background, and intuition
* Definition and Building Blocks
* Protocol Description
* Security Proof

## Product Backlog and Work Item Management

### Backlog approach

**Product backlog and Acceptance Criteria**

Our product backlog is ordered into features that represent chunks of work that are considered to be of value to the business. The requirements must be expressed in terms of acceptance criteria that define what the expected behavior of the system is. These acceptance criteria should be understandable and verifiable by people who are not members of the development team.

**Work item breakdown**
The team will further break down stories in the product backlog into work items (product backlog items) that have acceptance criteria that are understandable and verifiable by any member of the development team. These work items must be of an appropriate complexity that they can be effectively estimated by the development team. Beyond this the work items should be broken down in such a way that the development team decides who is most convenient to do the work.

**Spikes**
Spikes are defined by the engineering team as investigative work that they deem must be done in order to break down the business requirements into work items that are acceptable according to the above definition. Spikes MUST include a timebox. The output of a spike is either more spikes, a set of work items to fulfill the business criteria or a solution for the problem.

**Bugs**
Bugs should be recorded when anyone notices that the currently delivered product does not match the requirements of previously completed stories or in any way does not match the current definition of done for those requirements. Deficiencies discovered in work that was marked completed this sprint is not a bug but the deficiency must be resolved if the work is to be considered done at the end of the sprint. A bug must include the desired state and the actual state of the delivered product.

### Work approval and planning process
Work performed in engineering undergoes a quality gating process before it can be worked on.

Two core documents drive this work:
1. Product Requirements Document (PRD)
   > See also: [PRD Links](./scrum_leader_guide.md#prd-links).
2. Engineering Requirements Document (ERD)


#### Product-driven work

For work originating outside of Engineering:**
* The requirements come from a Product Requirements Document (PRD). 
* It is owned by the **Director of Product**.
* It **must be approved by the VP of Engineering or a delegate** before being worked.

**Core flow**
1. A requirements document is created by the product team, and approved with the required stakeholders
2. The document is submitted to the VP of Engineering for review. It may be sent back to Product for modifications. 
3. The VP of Engineering or delegate
 approve the document.
3. A Scrum leader receives the approved requirements and creates a design item to architect and plan the work (follow usual process for work estimation and planning).
4. The design plan is approved by the VP of Engineering or a Principal Engineer before backlog items can be created
5. Backlog items are created and estimated by the team


#### Engineering-driven work
For work originating within Engineering (e.g. optimizations, special initiatives, refactors, significant tech debt):
* The requirements come from an Engineering Requirements Document (ERD). 
* It is owned by the **Engineering team** and can be created by any engineer (the same engineering approvals apply as above).
* It **must be approved by the VP of Engineering or a Principal Engineer** before being worked.

**Core flow**
1. A Requirements Document is created and circulated for approvals (see below for types of requirements).
2. Once complete, the VP of Engineering or a Principal Engineer approves the requirements for Engineering.
3. A Scrum leader receives the approved requirements and creates a design item to architect and plan the work (follow usual process for work estimation and planning).
4. The design plan is approved by the VP of Engineering or a Principal Engineer before backlog items can be created
5. Backlog items are created and estimated by the team


### MR to ADO item mapping
Every MR should refer back to the ADO item that asked for the change. Every ADO item should contain links for relevant MRs.

## Azure Devops (ADO) Work Management

We use Azure Devops as our work management tool: [https://dev.azure.com/transparentsystems/xand-network]([https://dev.azure.com/transparentsystems/xand-network]) | [Documentation](https://docs.microsoft.com/en-us/azure/devops/?view=azure-devops)

We use the following taxonomy to contain work items. 

This is done to ensure that work items are not "orphaned" (i.e., not represented in an epic and feature and therefore hard to connect with overall business value delivered).

>Note: Epics and Features can also represent tech debt or other types of work not directly linked to a requirements document, such as retrospective follow ups or product launch activities.

### Work Types

1. Epics
2. Features 
3. Tasks
   1. Backlog item
   2. Spike
   3. Bug

#### Ownership
|               | Epic                   | Feature                | Product Backlog Item | Bug         |
|---------------|------------------------|------------------------|----------------------|-------------|
| **Ownership** | Product or Engineering | Product or Engineering | Engineering          | Engineering |


#### Epics
**Business Initiative -** Business level initiative will be represented by an Epic in Azure Devops.    

### Features
**Business Features -** Business level features are represented as Features in Azure Devops. A feature typically represents a shippable component of software. The feature should include either a full description of the requirements or a link to the Software Requirements Specification. If additional acceptance criteria are needed they will be included in the feature description. Features should not be modified without requirements owner involvement.

### Tasks
**Engineering Tasks -** Engineering tasks are represented as Product Backlog Items in Azure Devops. Stories must include acceptance criteria and an estimate before being accepted into a sprint. One exception is `unplanned` work items which can be taken as-needed based on the business need. 

**Spikes -** Spikes are represented as Product Backlog Items in Azure Devops with the `Spike [n days]` prefix in the title field.

**Bugs -** Bugs are represented as bugs in Azure.

## Pivotal Tracker

We used *Pivotal Tracker* in the past (pre-2020), if an item was migrated from that system it will include a link back for context.

### Tags

ADO tags can be used to sort items, especially in the service of backlog grooming.

- `needs priority` - added to new items upon creation, except where known. 
- `needs discussion` - may or may not be necessary.  It requires team discussion to see if it should be adopted. 
- `needs discovery` - requires spiking before it can be taken on. Link the spike as a predecessor if one exists.
- `needs ac` - requires Acceptance Criteria.
- `blocked` - This item has a finish-to-start constraint with a predecessor or has an external depdendency that hasn't resolved.
- `needs estimation` / `needs timebox` - This item or spike needs an estimation from the team.
- `unplanned` - This item is unplanned product support work and shouldn't be estimated but is high priority.
- `tech-debt` - we expect to allot up to 20% of the points in each sprint to tech-debt that is prioritized by the engineering team.

### Resources

See [links](./scrum_leader_guide.md#links)