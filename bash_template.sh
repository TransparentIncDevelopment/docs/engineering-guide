#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by [humans|automation]
    The purpose of this script is to serve as a template for other people to copy and 
    make their own bash scripts. This script serves no other function and is rather pointless.

    Arguments
        SOME_THING (Required) = Any value that will be used to do a thing
        SOME_NUMBER (Required) = A Numerical value
        SOME_FLAG (Optional) = If set to anything other than false do a thing. Defaults to false
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

if [[ $# -eq 0 ]] ; then
    error 'No arguments provided'
fi

SOME_THING=${1:?"$(error 'SOME_THING must be set' )"}
SOME_NUMBER=${2:?"$(error 'SOME_NUMBER must be set' )"}
SOME_FLAG=${3:-"false"}

#Do additional validation
re_digits='^[0-9]+$'
if ! [[ $SOME_NUMBER =~ $re_digits ]] ; then
   error 'SOME_NUMBER must be a number'
fi
