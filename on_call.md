# On Call

The Transparent On-Call production support team includes all engineering team members. New engineers will be added to the schedule at the end of the rotation after their start date to allow for onboarding. Engineers go on-call on a schedule that includes a backup on-call engineer in case the primary on-call responder is unable to respond.

Currently, all incident response takes place during business hours, 8AM-5PM PST. The goal of having all of our engineers participate on an on-call rotation is to provide timely incident response, and help share DevOps and operational knowledge across the team. Engineers are not expected to be able to solve any issue that arises, but rather they will make an initial attempt to resolve. Otherwise they will work with resolving engineer to coordinate response and solution.

# Responsibilities

The primary responsibility of Engineers who are on-call is to monitor and respond to production incidents. Currently, our production alerting happens in the `#support-alerting` slack channel. We intend to move this to an automated paging service before beta launches. 

On-Call Engineers should also monitor and triage support requests that happen in the TPFS Support Slack, and any emails requests that come to the support@transparent.us email distribution group.

> Note: engineers may also wish to consult other support-related channels for alerts.

Channels to consult:

> * `#support`
> * `#support-alerting`
> * `#engineering-support`
> * `#engineering-dev-alerts`
> * `#ci-issues`

On-Call Engineers are also expected to monitor development environments and CI/CD job failures as a severity 4 task.

For a complete list of responsibilities, see the [Incident Response Policy](https://transparentfinancialsystems.sharepoint.com/:w:/s/TransparentSystems/ES2y1NZOUBpHs20bVjESMuABL5fDyEQRMxYJlE3xOK94Bg?e=lMoxpd), which can be found in the [company OneDrive folder](https://transparentfinancialsystems.sharepoint.com/sites/TransparentSystems/Shared%20Documents/Forms/AllItems.aspx) under "Engineering/On Call".

# On-Call Resources

Please search within our [DevOps (Confluence)Runbooks](https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/259883051/Runbooks) for solutions/diagnosing to known issues.

Otherwise general information can be found in our [DevOps (Confluence)](https://transparentinc.atlassian.net/wiki/spaces/XNS/overview).

Where possible, engineers should add documentation for known fixes either in the indicated area, or in our internal Transparent Systems [Stack Overflow](https://stackoverflow.com/c/transparent/questions).


## Questions
Use the Slack channel ` #engineering-oncall` for questions not addressed in the documentation.

# Expectations

Engineers who respond to production incidents are expected to have access to production and development cloud enviornments. As of the date of writing, this requires:

  - Access to both [tpfssupport.slack.com](https://tpfssupport.slack.com/) and [transparentsystems.slack.com](https://transparentsystems.slack.com/) Slack instances
  - Access to the Development, xand-qa and Production Gcloud projects, along with the CI Azure and AWS subscriptions
  - Access to the [vpn.xand.tools](https://vpn.xand.tools/) VPN for whitelisting
  - Access to the TPFS 1Password and associated vaults
  - On-Call Engineers should be added to the support email group.

Engineers are expected to be familiar with general product operations and maintain their access as long as they are on the team.

While on-call, engineers are expected to make a best effort to solve incidents that happen on their own. Once this best effort is made, they are expected to work with others to lead a response. 

# Incident Management

Engineers are expected to open an Azure DevOps Incident ticket for each incident that occurs. These tickets are to be tracked in ADO with the tag "production-incident" and added to the [incidents team backlog](https://dev.azure.com/transparentsystems/xand-network/_boards/board/t/Incidents/Backlog%20items). 

In cases of severity 1-3 incidents, as defined by the [Incident Response Policy](https://transparentfinancialsystems.sharepoint.com/:w:/s/TransparentSystems/ES2y1NZOUBpHs20bVjESMuABL5fDyEQRMxYJlE3xOK94Bg?e=lMoxpd), Transparent Financial Systems management team members are expected to be notified immediately. 

# Schedule

Currently, the [on-call schedule](https://transparentfinancialsystems.sharepoint.com/:x:/s/TransparentSystems/EUrFRCsq9uxAvKbFcujtC8MBGwISkD4gdsb_jEOILEcVZQ?e=OS64rI) can be found in an Excel document in the [company OneDrive folder](https://transparentfinancialsystems.sharepoint.com/sites/TransparentSystems/Shared%20Documents/Forms/AllItems.aspx) under "Engineering/On Call". It is our intention to move this to a paging system before beta launches.
