# Source Control

We use Git for source control management: https://git-scm.com/doc

We use OneFlow as our branching strategy: https://transparentinc.atlassian.net/wiki/spaces/PD/pages/215515297/OneFlow

Our central source repository is GitLab: https://gitlab.com/TransparentIncDevelopment

## Repositories

Source code should be divided up into seperate repositories that represent deployable units for functionality. 

We take advantage of Gitlab's [Subgroups](https://docs.gitlab.com/ee/user/group/subgroups/) feature to following a naming convention and hierarchy for TPFS repositories. 

Currently, our top-level Subgroups within [`TransparentIncDevelopment` (link)](https://gitlab.com/TransparentIncDevelopment) are:
```
TransparentIncDevelopment
├── Docs
├── External
├── InternalTools
├── ITOps
├── Product
└── R+D
```

`Docs` houses intenral engineering documentation and acts as an area for shared content, like Guild notes. Treat `R+D` like a playground. You can create new repositories there as often as you'd like. 
`Product`, `ITOps`, and `InternalTools` all house code that support our day-to-day development process as well as the production components we ship to customers. `External` is intended for documentation to be shared externally. Treat code and docs within those repos with respect and care. 

A full expansion of the current repo hierachy with some sample repos filled in:
```
TransparentIncDevelopment
├── Docs
│   ├── architecture-guild
│   ├── engineer-guide
│   ├── learning-guild
│   └── security-guild
├── External
├── InternalTools
│   └── tpfs-crates
├── ITOps
│   └── sample-repo
├── Product
│   ├── Apps
│   │   ├── member-api
│   │   ├── trustee-node
│   │   ├── validator
│   │   ├── wallet
│   │   └── xand_models
│   ├── DevOps
│   │   └── deployment-ci
│   ├── Libs
│   │   ├── metrics
│   │   ├── threadsafe_lru
│   │   └── tpfs-logger
│   └── Testing
│       ├── acceptance-tests
│       └── bank-mocks
└── R+D
    └── poc-1
```

> Update and see the hierarchy tree using the [scripts/source_controls/mkdirs.sh](./scripts/source_control/mkdirs.sh) script

See the [`tpfs-ci`](https://gitlab.com/TransparentIncDevelopment/product/devops/tpfs-ci) for steps and required settings to create new repos under the `Product` subgroup.

## Code Reviews

Updates to integration branches per repo are done via the Merge Request process. For `thermite`, `develop` 
is the main integration branch. For other repos within the `Product` subgroup, changes get merged 
into `master`. 

Merge requests and code reviews are critical tools for us to ensure quality and facilitate the transfer
of knowledge between team members. Good review feedback is a two way street. Reviewers should work to 
present constructive, useful feedback and reviewees should prioritize collaborative spirit over ego. 
Review comments are not an attack on your code to be defended, they are a collective effort to keep our
quality high. If a discussion isn't going well in a code review, consider taking things offline for 
a longer discussion in person (if possible) or over the phone.

### MR Requirements

- Every MR must include an english sentence description of what the intention of the change is
- Every MR must be approved by 2 or more approvers before merging
- Every MR must pass the CI pipeline before merging

### Additional MR Guidelines

- Ensure your MR passes the deployment and verify phase. If these are broken, it will cause downstream issues for the rest of the team and needs to be addressed immediately. Ideally test your changes in a [review app](https://transparentinc.atlassian.net/wiki/spaces/PD/pages/277413889/Gitlab+CI+Review+Apps+Create+an+environment+for+your+branch+using+CI+CD) first.
- Keep MRs small where possible. It makes it more likely that your review will be prompt and complete. Keep in mind that you can always break up a work item into multiple MRs
- Separate refactoring MRs from functional changes. When doing significant refactors make every effort to separate those changes from functional changes so that the reviewer doesn't have to look at every line change and try and parse out if it's a functional change or just a rename.
- Review MRs promptly and merge them. This is the responsibility of the submitter and all of us to ensure that MRs get through the process quickly.
- Set an approver if you need feedback from someone specific
- Always check the remove branch / squash commits boxes
- If you want to make a comment but don't want it to block the merge, prefix your comment with "NIT:" so the reviewee knows they can mark it resolved if they need to merge and want to fix it later.
