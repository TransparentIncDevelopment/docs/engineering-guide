# Scrum Leader Guide
[[_TOC_]]

## Overview
Adapted from the Scrum Basics Guide (authored by Jake Casaletto) [link](https://gitlab.com/TransparentIncDevelopment/docs/tpfs-scrum-basics).

"Scrum Leader" is a hybrid role performed along with Engineering responsibilities.  Scrum leaders should delegate when possible, with volunteers first and then deputized colleagues second.  Scrum Leaders are not expected to be the team lead, but rather to serve as a facilitator to aid the team to stay organized and deliver on sprint commitments.  They should collaborate and document as much as possible, and provide a paper trail of decisions.

The Scrum co-leads are responsible for three key team meetings:

1. Standup (with a focus on delivery by end of sprint)
2. Backlog grooming
3. Planning

> See [meeting details](./work_management.md#meeting-summary).

**Out of scope:**

* Ensuring demo readiness*
* Product management
* Architectural design*
* Team management/supervision

_\*- *- this may be performed as an Engineer Individual Contributor_

> See [Viewing](#viewing-links) for the appropriate team board.


## Scrum Lead Responsibilities
At a high level, it is the scrum lead's job to ensure that:
* Meetings are scheduled and run smoothly
* The team knows what they're supposed to work on and how to find that work
* Work in the backlog is properly prioritized against business and engineering interests
* The ADO board and team are properly administered
* The team is delivering planned work at the end of the sprint commitment
* Smooth transitions for incoming scrum leads: permissions (calendar events, ADO, Slack, etc. ); offloading responsibilities from the outgoing scrum leads.

The team should be empowered to capture and document new work items, and it is the scrum leader's responsibility to help them do that by following the guidance here.

### Team facilitation
The scrum leader's responsibility is to:

* Run standup meetings
* Follow up on (or delegate) takeaway action items from owned meetings with a paper trail + properly organized ADO items
* Take note of impediments or road blocks the team is facing and address 1:1 if necessary to gather data, then plan or delegate followup actions to alleviate
* Keep Outlook schedules for owned meetings with the team up to date


### Backlog Grooming

> Conduct backlog grooming in alignment with the [backlog grooming guide](./work_management.md#backlog-grooming).

The scrum leader's responsibility is to:

* Keep the epics, features, and backlog items prioritized in ADO
* Ensure that each product backlog item / bug has a feature or epic parent; delegate where necessary
* Ensure that item tags (`needs discussion`, `needs priority`, etc) are kept up to date on all ADO items; delegate where necessary
* Catch untracked items (bugs or backlog items or features); ensure new items are homed in an Epic/Feature, and tracked in ADO with a `needs priority` tag.
* Review the ADO dashboard for the team's velocity to predict reasonable point velocities for sprints.

### Prioritization

#### Overview

The general list of priorities is as follows:

1. PRD/ERD-derived work
2. Bugs
3. SGD followup work
4. Retrospective followup work
5. Technical Debt*
6. Unplanned work**

\* - Technical debt is understood to be about 10% of the sprint's effort

\*\* - Unplanned work is not estimated, but the impact is measured through a lower velocity and via a dashboard metric


#### Prioritization Approach
Each level of items -- Epic, Feature, and Backlog Item/Bug -- must be prioritized in the backlog separately.

Suggested prioritization order:

1. Epics
2. Features (within parent Epic)
3. Backlog items

Prioritizing the backlog items / bugs is the most time consuming work for a scrum leader, and should be done in a grooming session instead of ad-hoc.  

It is most important to have a ready-to-plan backlog for reasonably upcoming work. If the backlog is hundreds of items long, then prioritizing the first 50 - 100 backlog items is sufficient.

Ensure that all items appearing on the backlog are indeed needed by the business, and that any unprioritized items are so-tagged, and handled in a timely manner.

> See also: [Bulk edit backlog items](https://docs.microsoft.com/en-us/azure/devops/boards/queries/add-tags-to-work-items?view=azure-devops)

> See also: [tags](./work_management.md#tags)

### Planning

> Conduct team planning in alignment with the [planning guide](./work_management.md#sprint-planning-meeting).

### Estimations
We use the Fibonacci numbering sequence for estimating our stories.

The scrum leader's responsibility is to:

* Ensure upcoming items are estimated by the team.
* Facilitate the estimation process with team by using data from:
  * Team velocity/dashboard
  * Prior estimations
  * Estimation point reference stories (Select/reinforce these as needed)

> **Important**: Each story needs to be estimated and each spike needs to be timeboxed before being committed in a sprint.

#### Estimation Meeting Process
We currently use ADO for estimation sessions.

We prefer the following estimation technique:

1. Scrum leader prepares an estimation session for unestimated items and sends out the session link to the team
2. Everyone asynchronously comes up with an estimate
3. Team meets and reveal estimates for one item at a time, aiming to reach consensus through discussion
4. Team selects an estimate
5. Scrum lead updates the story with the effort level/timebox

> Note: Under exceptional circumstances (e.g. unblocking high priority/high urgency work), we may also elect to estimate via an ad hoc estimation meeting, or Slack thread

> See also: [ADO Estimations](#estimating-items-in-ado)

#### When Should We Estimate
Create an estimation session if there is work that is coming up within the next couple of sprints that has the tag `needs estimation`. Ad hoc incoming work should be estimated expeditiously if it is of high priority. 

If work is generated from a design process, that work should be estimated in the next estimation session, soon after being created in ADO. This helps make sure the design work is most fresh in mind for accurate estimations.

### Work Quality Gating
The lifecycle of a work item is defined in [Work Approval](./work_management.md#work-approval-and-planning-process).

The scrum leader's responsibility is to:

* Maintain a general awareness of product priorities and PRDs/ERDs in the pipeline.
* When PRDs/ERDs are given final approval, create backlog item(s) for the design process, facilitate estimation and getting the appropriate inputs/expertise for the process.
* Ensure that work items are created in ADO (if multiple Features exist, parent them under an Epic).
* Once a feature is designed, work items broken out and approved, work items created in ADO, and the effort planned, prioritize the work in the backlog against other items.
* Ensure that work is not planned/committed in a sprint without acceptance criteria defined and an estimate given.

#### Guidance on Design
>Note: It is not the scrum leaders' responsibility to facilitate the design of features, however they may do so in their capacity as Engineers. This info has been provided to help team members estimate and carry out design activities. Substantive Design and Architecture questions should be addressed with the Architecture guild. 

##### Process
New product features follow a process instead of being added to the backlog ad-hoc.

##### Organization
If a product feature is coming from the Product team and a PRD, it's likely that multiple ADO feature items will be required and an Epic should be used to track the item.  If a team member wants to add a single feature for a collection of backlog items, add it to an existing Epic if appropriate, or make a new one for it.

[Here](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Features/?showParents=false&workitem=7334) is an example of an item which was given a PRD, then designed and the effort planned.

##### Definition
First, the Feature must be defined.  

Any work from product is represented by a PRD. 

Any work from engineering is represented by an ERD. 

> See also: [Work approval](./work_management.md#work-approval-and-planning-process)

##### Design + Approval
Once there are requirements defined for the work, the owner of the design/architecture task should get 2-3 volunteers to help architect a solution which fulfills the requirements.  Depending on the complexity of the problem, a proper design can be as complex as a diagram on Miro or as simple as a written explanation of what change needs to occur.  

After the design is approved, volunteers from the team should meet and plan the effort for the implementation of the design.  That means breaking the design into ADO feature and backlog item sets which are reasonably scoped with descriptions and acceptance criteria.

**Some Tips on Design and Work Breakdown:**

1. Try to keep effort parallelizable so that the work can be accomplished simultaneously.
1. Track the predecessor and successor of every story and make sure to document any blocking dependent items which have a finish-to-start constraint.  A solid paper-trail is important.
1. Consider that the [definition of done](./work_management.md#definition-of-done) for a feature includes updating our deployments and tests.

[This Miro board](https://miro.com/app/board/o9J_lENK6pU=/) contains examples of the effort planning for Bank Transaction Repository.  (under "planning" on the right side of the board).

##### Design Output
The output of the design process must:
* be sufficient for other engineers to understand the design of the solution
* facilitate informed discussions about implementation details
* produce work items that are well-defined, prioritized, scoped, accurate to the requirements, and as parallelized as possible

###### Requirement for Working on Items
1. Design created
2. Design reviewed and approved by a principal engineer
3. Backlog items created

Follow the usual process for estimating and planning work items.

## Communication Tools
### Slack
* Use the `@scrum` alias to alert the current Scrum Leaders

### Outlook
* Scrum leads should create all owned meetings in the `Scrum Leads` (scrumleads@transparent.us) Outlook calendar


## Rotations
Scrum co-leaders serve a 6 sprint term. Two scrum leads should serve terms simultaneously, with their start dates overlapping each other by 3 sprints, as defined [here](https://transparentfinancialsystems.sharepoint.com/:w:/s/TransparentSystems/EV0v8maCJapAjfC3BG9GwdMBoiHmePqIiJ1waK8r7YapCQ?e=xy5hd7).

### Housekeeping at Start/End of Rotation 
When leads rotate on/off, the following memberships need to be adjusted to add/remove the individual

1.  Slack `@scrum` group
2.  Outlook `Scrum Leads` group/calendar: Add _delegate_ permissions to allow leads to create/edit/delete events on the calendar. More information can be found on shared mailbox calendars and their use [here](https://support.microsoft.com/en-us/office/open-and-use-a-shared-mailbox-in-outlook-on-the-web-98b5a90d-4e38-415d-a030-f09a4cd28207#__how_can_do) You can also reach out to IT if you have questions.
3.  ADO Team Admin membership


## Flow of a Sprint
### Example of a Typical Sprint Schedule

| MON	                                                                                                | TUE	                                                                                                                    | WED	                                                                                  | THUR	                                                                                         | FRI	                                              |
|:----------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------|:--------------------------------------------------|
| 1) **Planning**. <br> Begin the sprint.                                                             | General duties<br>(good time to start prioritizing any retrospective followups, making agenda/updates for sprint sync)	 | General duties <br> 2) **Scrum Sync with VP of Engineering**	                         | General duties (good time to delegate any new task discovery)	                                | General duties (good time to check on dashboard)	 |
| General duties <br> (good time to check with team on sprint progress)	                              | General duties <br> (good time to check in on ERD/PRD status and team progress in current sprint)	                      | General duties <br> (good time to delegate extra help to complete sprint commitment)	 | Participate in **Retrospective** - Ensure Followup Items in ADO <br> 3) **Backlog Grooming**	 | 4) **End the sprint**	                            |


## ADO Overview
This is not an exhaustive guide on how to use ADO, please read official [ADO Documentation](https://docs.microsoft.com/en-us/azure/devops/?view=azure-devops).

>Note: For specific ADO boards/views, see the [Viewing](#viewing-links) section.

### Dashboard
Use the dashboard to track velocity, unplanned work, and turnaround time for the team.  Feel free to edit the dashboard. Some panels may require periodic manual updates to set lookback time periods. 


### Backlog
This is the main view used for backlog grooming. The ordering of items in this view will persist in the board view (unless this is changed as an ADO setting).

All items in the backlog must be parented to features and epics.  It is important to keep this backlog up to date and prioritized, as it is the single source of truth for team priorities.

> Note: Keeping the first 50-ish items prioritized is of higher value than continuously pruning the entire backlog.

### Epic Backlog: Optional 
It may be helpful to keep this up to date and prioritized, for the purposes of backlog grooming.

## ADO Item Management
### Creating new ADO Items
To create a new item (backlog or bug):

1. Find the appropriate Epic and Feature, then add it as a child item
2. If the Epic or Feature do not exist, create them, then add the child item.
3. Tag the new item(s) with `needs-priority` unless this is otherwise known.
4. If needed, notify a Scrum leader(s) via [Communication Channels](#communication-tools) so it can be properly triaged. 

> See also: [tags](./work_management.md#tags).

Work that comprises multiple features (e.g. a larger initiative with multiple units of business value delivered) will require an Epic to group the features.

> See also: [work types](./work_management.md#work-types)

> Note: Make sure that there are no "floating" backlog items or bugs and that each one has a feature or epic parent.  These usually appear at the bottom of the backlog view. 

### Estimating Items in ADO
We prefer the following estimation method:
**ADO "Offline" Estimation Board + Synchronous Estimation Meeting.**

> Note: In the case of ad hoc estimations, use Zoom or Slack estimation over ADO "Online" estimations, as the format is not user friendly.
> 
### How to Set Up an Estimation Session
* In a backlog view, select one or multiple (Ctr+Click) product backlog items for a single estimation session
* Click the triple-dot icon on the right side of any of the items
* Select "Estimate Work Items"

![](assets/starting-estimation.png "Starting an Estimation")

* When setting up the session, select a descriptive name
* Select "Offline" for the session type, so that votes are saved over time
* Choose the "Fibonacci" card sequence
* Click "Create" to make the session 

>Note: A user must have project-level admin privileges to create estimation sessions, see also: [Project Config](#configure-project-settings) and [Admin Links](#administration-links). If you do not have the privileges required, see a team lead or IT for assistance.

![](assets/estimation-settings.png "Starting an Estimation")

* Discuss each ADO item and vote on an estimate (# = points) or timebox (# = days) by clicking on a number.
![](assets/estimate-vote.png "Vote on Estimate")
> Note: if you are sharing your screen/created the session, you should vote last, because your vote will be revealed immediately
* Discuss the group's estimates, see guidance [here](#estimations). 
* Once decided, select a final estimate and click "Save", which will update the item's effort:
![](assets/estimate-selection.png "Select Estimate")
* Remove the `needs estimate` or `needs timebox` tag from the ADO item
* After the item has been estimated and the session is no longer needed, click the triple-dot icon at the top of the session name and select "End Session" to delete it from the list of active estimation sessions.

![](assets/end-session.png "Ending an Estimation Session")

### Creating an Iteration
In order to properly use ADO, scrum leads need to define and assign iterations to a team. This allows ADO to detect which iteration is the `@CurrentIteration`, for the purpose of viewing the board, editing stories, and measuring effort.

#### Numbering Scheme
We use the `year.week` numbering scheme, starting with `01` at the beginning of a new calendar year. 

We start numbering on week "1", and sprints are two weeks long, therefore sprint numbers are odd.

> Example: the first iteration of 2023 would be `23.01`.

#### Creating New Iterations in ADO 
ADO Assigns iterations from a schedule that is configured at the global level in the [work settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work) window.

1. From the main Xand-network project [page](https://dev.azure.com/transparentsystems/xand-network), click on 'Project Settings' (usually in the bottom left hand corner)

![](assets/project-settings.png "Project Settings Button")
2. Click on "Project configuration" from the left navigational menu

![](assets/project-configuration.png "Project Configuration")

3. Create a `New Child` under the V1 iteration group (which we have been using until the V1 release)
4. Name the iteration using the naming scheme above

![n](assets/new-v1-iteration.png "New Child Iteration Under V1")

5. Choose a Monday start date for the sprint and ADO will automatically create a 2-week end date

 ![](assets/new-iteration-dialog.png "Example New Iteration")

> Note: The ADO UI allows you to make multiple child iterations at once

#### Assigning Iterations to a Team
After the iteration is created, the team needs to be assigned to the new iterations in the [default team's settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work-team?_a=iterations).

1. From the main Xand-network project [page](https://dev.azure.com/transparentsystems/xand-network), click on 'Project Settings' (usually in the bottom left hand corner)

![](assets/project-settings.png "Project Settings Button")
2. Click on "Team Configuration" from the left navigational menu

![](assets/team-configuration.png "Team Configuration" )

3. Click on "Iterations" from the section's navigational pane

![](assets/iterations-subpane.png "Iterations Settings")

4. Click on `Select Iteration(s)`

![](assets/new-team-iterations.png "New Team Iterations")

5. Choose any number of globally-created iterations from the dropdown (e.g. `V1\22.17`)
6. Confirm the choice

> Note: Do not simply assign the iteration's parent, as this will over-write any existing iterations from that parent, and will not import all the child objects. You have to manually add iterations given this ADO limitation.

### Committing to Sprint Items

When planning a new sprint, we commit to items for a sprint by adding them to that iteration.

>**Important**: Ensure this is done by the end of the first working day of a sprint. Items committed to after the end of the first working day of a sprint will not be counted toward the sprint commitment vis-a-vis team velocity and other metrics. Read more about [Sprint Configuration](#configure-team-settings).


1. In ADO Backlog view, Ctrl+Click items to be included in a sprint
2. Right click the "meatball" icon next to one of the items, and select "Move to Iteration", select iteration
   ![](assets/move-to-iteration.png)

### Tags
> See also: tag meanings in [work management](./work_management.md#tags).

The most common tag lifecycle of an item is

> `needs priority` -> 
>
> `needs discovery` -> 
>
> `needs ac` -> 
> 
> `needs estimation`  
> 
> (prioritized, specified, defined, estimated item = can be worked on)

Remove tags that no longer apply to the item. 

### Blocked States
#### How to Block an Item
If an item is blocked due to:
* **Factors external to engineering**: Move the item into the "Blocked" column
  * **Result**: the item is counted as "new" and does not contribute to metrics on items in-progress

    ![](assets/blocked-column.png "Blocked Column")
   
* **Factors internal to engineering (e.g.: dependency not met)**: Tag the item with `blocked` and move it into the "New" column
  * **Result**: it is still counted as an item in-progress, however there is a visual reminder that it has an impediment to its progress

    ![](assets/blocked-state.png "Blocked State")

#### Clarification on Blocked Meaning

The `blocked` "tag", Blocked "state", and "Blocked" column are separate concepts.

* **`blocked` tag**: Used to mark internally-blocked stories (e.g. waiting on another story to finish, pipeline to pass, etc)

* **Blocked state**: Used to mark externally-blocked stories' state. When set to Blocked, ADO will block the completion of any dependant stories. 

* **Blocked column**: Used to locate externally-blocked stories on the Kanban board. Often used for `unplanned` work as a means to having continued visibility.

> Note: Some undocumented behavior occurs when moving the item between Blocked and Doing columns. This may be due to how our "Blocked" column is mapped to "New":

> ![](assets/blocked-column-settings.png "Blocked Column Settings")

The blocked state can be configured in the [Scrum Process Settings](https://dev.azure.com/transparentsystems/_settings/process?process-name=Transparent%20Scrum&type-id=TransparentScrum.ProductBacklogItem&_a=states)

Read more about customizing states: [Customizing Process Fields](https://docs.microsoft.com/en-us/azure/devops/organizations/settings/work/customize-process-field?view=azure-devops).

Read more about customizing columns: [Add and Customize Columns in Kanban Boards](https://docs.microsoft.com/en-us/azure/devops/boards/boards/add-columns?view=azure-devops)

## ADO Administration
Sprints in ADO are called "iterations". Create a new iteration in the Work Team Iterations settings view. Then, assign them to the team under the default team iterations view.


### Configure Team Settings
Go to the "Team Configuration" panel of the Project Settings Dashboard. 
> See link in [team settings](#team-settings-links).

This allows customization of:
* Team members
* Team name
* Working days
* Owned iterations (sprints)
* Default Area Path (grouping for the team's work items)
* Task templates

### Configure Admin Settings
Go to the "Overview" panel of the Project Settings Dashboard. 
> See link in [Administration](#administration-links).

This allows customization of:
* Scrum admin permissions
* Project name
* Assigned ADO services

### Configure Board Settings
Go to the "Settings" view of the ADO Board, click on the gear icon

![](assets/board-settings.png "Board Settings")

This allows customization of:
* Card styling
* Tag colors
* Board columns
* Card ordering rules (when moving between columns)
* Working days
* Bug settings

#### Configure Tags

1. From the Board Settings pane, click on "Tag colors"
2. Customize the tags and colors as needed:

![](assets/tag-colors.png "Tag colors")

> See also: [Customize Tags](https://docs.microsoft.com/en-us/azure/devops/boards/boards/customize-cards?view=azure-devops)

### Configure Project Settings
Go to the "Project Configuration" panel of the Project Settings Dashboard. 
> See link in [project settings](#project-settings-links).

This allows customization of:
* All iterations for all teams
* Area path settings for all teams

> See also: [Manage Board Settings](https://docs.microsoft.com/en-us/azure/devops/boards/get-started/manage-boards?view=azure-devops)


## Links
> Note: Links are provided for the `Cookie Monster Team`.

### Viewing Links
* [Team Cookie Monster Backlog Board](https://dev.azure.com/transparentsystems/xand-network/_boards/board/t/Cookie%20Monster/Backlog%20items?showParents=false) - Filter this for @CurrentIteration to create a board view of the current sprint.

* [Team Cookie Monster Backlog Item View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Backlog%20items/) - List of backlog items.

* [Team Cookie Monster Features Backlog View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Features/) - List of Features the team is tracking for completion.

* [Team Cookie Monster Epics Backlog View](https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Cookie%20Monster/Epics/) - List of Epics the team is tracking for completion.  

### Reporting Links
* [Team Cookie Monster Dashboard](https://dev.azure.com/transparentsystems/xand-network/_dashboards/dashboard/0629de50-5111-46e0-8f2b-1230eab2fa2c) - Team dashboard which collects useful metrics and links.

### Administration Links
* [ADO Admin Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/projectOverview)

* [ADO Global Iteration Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work)  - Where iterations are created.

* [Team Cookie Monster Iteration Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work-team?_a=iterations&teamId=30fbba25-fea0-4862-a036-eebab482fc5a) - Where iterations are assigned to teams.

### Team Settings Links
* [Team Cookie Monster Team Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work-team)

### Project Settings Links
* [Xand Network Project Settings](https://dev.azure.com/transparentsystems/xand-network/_settings/work)

### PRD Links 
> Note: Owned by Product
* [PRD Miro Board](https://miro.com/app/board/o9J_lozxBm8=/) - Single Source of Truth for PRD review state. 

* [V1 PRD Sharepoint Folder](https://transparentfinancialsystems.sharepoint.com/:f:/s/TransparentSystems/EgA_7QZSfNtHvI6Relvow_oB9FOMKQzJAlc_LnGu6qeSXw?e=dha1Jw) - Text of PRDs. Inclusion in this folder does not guarantee that a PRD has been approved for Engineering. 