# Developer Experience

Part of working in a high-quality project is knowing how to build/test every piece of the project.

## README

Every repository should have a `README.md` file at the root that provides the following information.

- What is the purpose of this component.
- What prerequisites or dependencies are needed to work with it/run it
- How to build, test, and run the component

## Build Verbs

Every buildable component in the repo should support the following build verbs:

* **clean** - Removes any build artifacts associated with the component.
* **deps** - Builds any dependencies associated with the component*
* **build** - Compiles or Transpiles the component.
* **rebuild** - `clean` -> `deps` -> `build`
* **test** - Executes unit tests associated with the component*
* **lint** - perform any linting associated with the component
* **fix** - auto-correct any linting issues associated with the component.
* **verify** - `rebuild` -> `lint` -> `test`

> Given that cargo uses `run` to execute an application and `yarn` uses `run` to ask which build verb to execute, it is
not viable to standardize on a single verb for both rust and typescript projects.

* **run** - (Rust applications only) executes the application. In general apps should run with intelligent defaults for settings.
* **launch** - (Typescript applications only) executes the application. In general apps should run with intelligent defaults for settings.


## Build Tools

If the project is written in typescript, these verbs should be executable by `yarn`. If the project is written in rust, these verbs should be executable by `cargo` or `cargo make`.

## Execution of Build Commands
These commands should be executable at the root of the project directory (e.g., running `cargo build` in the `/ap-api` folder should build the `ap-api`.) If the commands are run in a parent directory, then every child project contained in the parent directory should be recursively executed as well. (E.g., running `cargo make build` at the `/thermite` root should cause every project in `/thermite` to be rebuilt. Likewise, running `cargo make build` in the `/small_crates` directory should cause all of the `/small_crates` project to be rebuilt.)