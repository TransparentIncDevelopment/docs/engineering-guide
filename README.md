Transparent Systems Engineering Guide
=============================

Purpose
-----------------------------

The purpose of this document is to provide a set of working agreements and best practices for engineering at Transparent Financial Systems. The principles and guidelines laid out here are intended to facilitate our company values of "Collaborate Effectively" and "Deliver With Quality". Engineers are expected to uphold the standards laid out in this document.

License
-----------------------------
MIT OR Apache-2.0

Amendments and Modification
-----------------------------

This guide is intended to be a living document and can be amended through the merge request process. Disputes are intended to be resolved via rough consensus. In the case rough consensus cannot be achieved decision making reverts to the cohort lead.

Contents
-----------------------------


- [Software Architecture and Testing](software_architecture_and_testing.md)
- [Source Control](source_control.md)
- [Versioning](versioning.md)
- [Design Review](design_review.md)
- [Work Managment](work_management.md)
- [Developer Experience](#developer-experience.md)
- [Observability](Observability.md)
- [Installation Paths](installation_paths.md)
