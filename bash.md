# Bash Guidelines

Bash is a commonly used scripting language on *nix based systems. For our purposes it is appropriate to use Bash for simple operational tasks. If your script starts to get complex you should consider building a simple command line tool and calling it from your script. It is best not to write anythong complex is Bash as it is nearly impossible to test and has [numerous common pitfalls](https://mywiki.wooledge.org/BashPitfalls).

## Basic Principles

- All scripts should have a help text that tells what the script is for and how it should be called
- The help text should be printed if the script is called with no arguments
- If any validation fails the validation message is printed followed by the help text
- Extract arguments into named variables and perform validation befor any script actions
- Be careful about expecting dependencies to be present. We should typically only use tools available on every instance of Ubuntu

## Basic Template

